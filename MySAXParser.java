package XMLparsing;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MySAXParser extends DefaultHandler {

    private boolean isUsername = false;
    private boolean isAge = false;
    private boolean isBirthday = false;
    private boolean isMood = false;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("user")) {
//            String  = attributes.getValue("");
            System.out.println("Encountered start element :"+ qName);
        } else if (qName.equalsIgnoreCase("username")) {
            isUsername = true;
        } else if (qName.equalsIgnoreCase("age")) {
            isAge = true;
        } else if (qName.equalsIgnoreCase("birthday")) {
            isBirthday = true;
        } else if (qName.equalsIgnoreCase("mood")) {
            isMood = true;
        }
    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("user")) {
            System.out.println("End Element :" + qName);
        }
    }

    public void characters (char ch[], int start, int length)
            throws SAXException
    {
        if (isUsername) {
            System.out.println("User Name: "
                    + new String(ch, start, length));
            isUsername = false;
        } else if (isAge) {
            System.out.println("Age: " + new String(ch, start, length));
            isAge = false;
        } else if (isBirthday) {
            System.out.println("Birth Date: " + new String(ch, start, length));
            isBirthday = false;
        } else if (isMood) {
            System.out.println("Mood: " + new String(ch, start, length));
            isMood = false;
        }
    }

    public void startDocument ()
            throws SAXException
    {
        System.out.println("Encountered start of doc.");
    }

    public void endDocument ()
            throws SAXException
    {
        System.out.println("Encountered end of doc.");
    }
}
