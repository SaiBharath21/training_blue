package XMLparsing;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXExample {
    public static void main(String[] args) {

        try {
            File inputFile = new File("C:\\Users\\saibh\\IdeaProjects\\Training\\src\\XMLparsing\\note");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            MySAXParser parserHandler = new MySAXParser();
            saxParser.parse(inputFile, parserHandler);
            parserHandler.startDocument();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
