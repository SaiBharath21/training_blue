package XMLparsing;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class DOMExample {


    public void parseXml() {
        try {

            File fXmlFile = new File("C:\\Users\\saibh\\IdeaProjects\\Training\\src\\XMLparsing\\note");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);


            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("note");

            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Element :" + nNode.getNodeName());
               /* System.out.println("Node Type :" + nNode.getNodeType());
                System.out.println(Node.ELEMENT_NODE);*/

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

//                    //System.out.println("Staff id : " + eElement.getAttribute("id"));
                    System.out.println("To : " + eElement.getElementsByTagName("to").item(0).getTextContent());
                    System.out.println("From : " + eElement.getElementsByTagName("from").item(0).getTextContent());
                    System.out.println("Heading : " + eElement.getElementsByTagName("heading").item(0).getTextContent());
                    System.out.println("Body : " + eElement.getElementsByTagName("body").item(0).getTextContent());

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        DOMExample dome = new DOMExample();
        dome.parseXml();
    }
}


