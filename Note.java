package XMLparsing;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class Note {
    String to;
    String from;
    String head;
    String body;
    @Override
    public String toString() {
        return "Note [to=" + to + ", from=" + from + ", head=" + head + ", body=" + body + "]";
    }
    public String getTo() {
        return to;
    }

    @XmlElement
    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    @XmlElement
    public void setFrom(String from) {
        this.from = from;
    }

    public String getHead() {
        return head;
    }
    @XmlElement
    public void setHead(String head) {
        this.head = head;
    }

    public String getBody() {
        return body;
    }

    @XmlElement
    public void setBody(String body) {
        this.body = body;
    }


}
