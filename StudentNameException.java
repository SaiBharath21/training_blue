package customException;

import java.util.Scanner;
public class StudentNameException {
    String[] studentName;
    public StudentNameException()
    {
     studentName= new String[]{"Rishi","Pruthvi","Rahul","Faizan","Sunny","Sai"};
    }
    public void searchName(String nameQuery) throws NameNotFoundException {
        int flag = 0;
        for (int i = 0; i < studentName.length; i++) {
            if (studentName[i].equals(nameQuery)) {
                System.out.println("Student found at " + i);
                flag = 1;
                
            }
        }
        if (flag == 0) {
            throw new NameNotFoundException("Name not found!");

        }
    }
    public static void main(String[] args) {
        StudentNameException studentNameException=new StudentNameException();
        String nameQuery;

        Scanner inp=new Scanner(System.in);
        System.out.println("Student Name Exception");
        System.out.println("----------------------");
        System.out.println("Enter student name to be searched:");
        nameQuery=inp.nextLine();
        System.out.println("...Searching Name:"+nameQuery+"...");

        try {
          studentNameException.searchName(nameQuery);
        }
        catch(NameNotFoundException N)
        {
            System.out.println("Student not found");
        }

    }
}
