package arrayOperations;

public class ArrayOperations {
    private int[] arr;
    public ArrayOperations()
    {
         arr=new int[]{5,8,45,33,3,-1,4,7,1,9,34,11,21,26};
    }
    void bubbleSort()
    {
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {
                    // swap temp and arr[i]
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }

 void secondMin()
 {
     System.out.println("The second minimum element is :"+arr[1]);
 }
    void printArray()
    {
        int n = arr.length;
        for (int i=0; i<n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }

    public static void main(String args[])
    {
        ArrayOperations arrayOperations = new ArrayOperations();
        System.out.println("Original array");
        arrayOperations.printArray();
        arrayOperations.bubbleSort();
        System.out.println("Sorted array");
        arrayOperations.printArray();
        arrayOperations.secondMin();
    }
}
