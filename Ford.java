import java.util.Scanner;
//https://github.com/Microsoft/Git-Credential-Manager-for-Windows/issues/710
interface carModel
{
	final int numWheels =4;
	void carDisplay();
}

public class Ford implements carModel {
	public void carDisplay()
	{
		System.out.println("Ford welcomes you:");
	}
	 public static void main(String[] args) {
   
        System.out.println("Java Interface Example");
	    Ford fordObj = new Ford();
		Honda hondaObj= new Honda();
		
		fordObj.carDisplay();
		hondaObj.carDisplay();
		
		
		
    }
    
}
 class Honda implements carModel {
	public void carDisplay()
	{
		System.out.println("Honda : Konnichiwa ");
	}
}
   

