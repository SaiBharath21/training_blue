import java.util.Scanner;
public class Calc {
	int a;
	int b;
	
	public Calc(int x,int y )
	{
		 this.a = x;

		 this.b = y;
	}
	public int add()
	{
		return a+b;
	}
	public int sub()
	{
		return a-b;
	}
	public int mul()
	{
		return a*b;
	}
	public int div()
	{
		return a/b;
	}

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Calculator :");
		int x,y;
		Scanner inputReader = new Scanner(System.in);
		
				 System.out.println("Enter num 1:");

		 x = inputReader.nextInt();
		 System.out.println("Enter num 2:");

		 y = inputReader.nextInt();
		Calc obj1= new Calc(x,y);
		
		
		System.out.println("Calculation results :");
		
		System.out.println("Add :"+obj1.add());
		System.out.println("Sub :"+obj1.sub());
		System.out.println("Mul :"+obj1.mul());
		System.out.println("Div :"+obj1.div());
		
		
		
    }
    

}